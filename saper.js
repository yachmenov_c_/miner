/**
 * Created by yaroslav on 09.02.16.
 */
function Saper(canvas_id, sq_rows, sq_cols, _mins, mine_id, flag_id){
    var canvas_obj = document.getElementById(canvas_id);
    var mine_obj = document.getElementById(mine_id);
    var flag_obj = document.getElementById(flag_id);
    var canvas = canvas_obj.getContext("2d");
    var height = parseInt(canvas_obj.height);
    var width = parseInt(canvas_obj.width);
    var bl_h = height/sq_rows;
    var bl_w = width/sq_cols;
    var mins = _mins;
    var sounds = {
        land: document.getElementById('land'),
        explosion: document.getElementById('explosion'),
        death: document.getElementById('death'),
        sand: document.getElementById('sand'),
        out_sand: document.getElementById('out_sand'),
        win: document.getElementById('win')
    };

    var field = new Array(sq_rows);
    var flag_field = new Array(sq_rows);
    for(var i = 0; i < sq_rows; i++) {
        field[i] = [];
        flag_field[i] = [];
        for (var j = 0; j < sq_cols; j++) {
            field[i].push(0);
            flag_field[i].push(0);
        }
    }

    canvas_obj.addEventListener('click', doLeftClick, false);
    canvas_obj.addEventListener('contextmenu', doRightClick, false);

    this.Draw = function(){
        canvas.fillStyle = 'lightgrey';
        canvas.strokeStyle = 'blue';
        for(var i = 0; i < sq_rows; i++){
            for(var j = 0; j < sq_cols; j++){
                canvas.beginPath();
                canvas.rect(j*bl_w,i*bl_h,bl_w,bl_h);
                canvas.fill();
                canvas.stroke();
                canvas.closePath();
            }
        }
        calculate();
    };

    function calculate(){
        for(var i = 0; i < mins; i++){
            while(true){
                var ir = Math.floor(Math.random() * sq_rows);
                var jr = Math.floor(Math.random() * sq_cols);
                if(field[ir][jr] != '*'){
                    field[ir][jr] = '*';
                    if(ir+1 < sq_rows) if(field[ir+1][jr] != '*') field[ir+1][jr]++;
                    if(ir-1 > -1) if(field[ir-1][jr] != '*') field[ir-1][jr]++;
                    if(jr+1 < sq_cols) if(field[ir][jr+1] != '*') field[ir][jr+1]++;
                    if(jr-1 > -1) if(field[ir][jr-1] != '*') field[ir][jr-1]++;
                    if(ir+1 < sq_rows && jr+1 < sq_cols) if(field[ir+1][jr+1] != '*') field[ir+1][jr+1]++;
                    if(ir-1 > -1 &&  jr-1 > -1) if(field[ir-1][jr-1] != '*') field[ir-1][jr-1]++;
                    if(ir-1 > -1 && jr+1 < sq_cols) if(field[ir-1][jr+1] != '*') field[ir-1][jr+1]++;
                    if(ir+1 < sq_rows && jr-1 > -1) if(field[ir+1][jr-1] != '*') field[ir+1][jr-1]++;
                    break;
                }
            }
        }
    }

    function doLeftClick(event){
        var coords = canvas_obj.getBoundingClientRect();
        var x = event.pageX - coords.left;
        var y = event.pageY - coords.top;
        for(var i = 0; i < sq_rows; i++){
            for(var j = 0; j < sq_cols; j++){
                canvas.beginPath();
                canvas.rect(j*bl_w,i*bl_h,bl_w,bl_h);
                if(canvas.isPointInPath(x, y)){
                    if(flag_field[i][j]) break;
                    switch(field[i][j]){
                        case -1:
                            break;
                        case 0:
                            canvas.closePath();
                            recursiveClearSq(i,j);
                            sounds.land.play();
                            break;
                        case '*':
                            canvas.fillStyle = 'red';
                            canvas.fill();
                            canvas.drawImage(mine_obj, j*bl_w, i*bl_h, bl_w, bl_h);
                            canvas_obj.removeEventListener('click', doLeftClick, false);
                            canvas_obj.removeEventListener('contextmenu', doRightClick, false);
                            field[i][j] = -1;
                            canvas.closePath();
                            sounds.explosion.play();
                            alert('Loser!');
                            sounds.death.play();
                            drawAnotherBombs();
                            break;
                        default:
                            drawText(i, j);
                            sounds.land.play();
                            break;
                    }
                    break;
                }
            }
        }
        checkForWin();
    }

    function doRightClick(event){
        event.preventDefault();
        restoreStyles();
        var coords = canvas_obj.getBoundingClientRect();
        var x = event.pageX - coords.left;
        var y = event.pageY - coords.top;
        for(var i = 0; i < sq_rows; i++) {
            for (var j = 0; j < sq_cols; j++) {
                canvas.beginPath();
                canvas.rect(j * bl_w, i * bl_h, bl_w, bl_h);
                if (canvas.isPointInPath(x, y)) {
                    if (!flag_field[i][j] && field[i][j] != -1) {
                        canvas.drawImage(flag_obj, j * bl_w, i * bl_h, bl_w, bl_h);
                        flag_field[i][j] = 1;
                        sounds.sand.play();
                    } else if (field[i][j] != -1) {
                        canvas.fill();
                        canvas.stroke();
                        flag_field[i][j] = 0;
                        sounds.out_sand.play();

                    }
                }
                canvas.closePath();
            }
        }
        checkForWin();
    }

    function recursiveClearSq(i, j) {
        recursiveIf = function(i, j){
            if (!field[i][j]) recursiveClearSq(i, j);
            else{
                if(field[i][j] != -1 && field[i][j] != '*')
                    drawText(i, j);
            }
        };
        canvas.beginPath();
        canvas.rect(j*bl_w,i*bl_h,bl_w,bl_h);
        canvas.fillStyle = 'white';
        canvas.fill();
        canvas.strokeStyle = 'green';
        canvas.stroke();
        canvas.closePath();
        field[i][j] = -1;
        if (i + 1 < sq_rows)
            recursiveIf(i + 1, j);
        if (i - 1 > -1)
            recursiveIf(i - 1, j);
        if (j + 1 < sq_cols)
            recursiveIf(i, j + 1);
        if (j - 1 > -1)
            recursiveIf(i, j - 1);
        if (i + 1 < sq_rows && j + 1 < sq_cols)
            recursiveIf(i + 1, j + 1);
        if (i - 1 > -1 && j - 1 > -1)
            recursiveIf(i - 1, j - 1);
        if (i - 1 > -1 && j + 1 < sq_cols)
            recursiveIf(i - 1, j + 1);
        if (i + 1 < sq_rows && j - 1 > -1)
            recursiveIf(i + 1, j - 1);
    }

    function drawAnotherBombs(){
        for(var i = 0; i < sq_rows; i++)
            for(var j = 0; j < sq_cols; j++)
                if(field[i][j] === '*' && !flag_field[i][j])
                    canvas.drawImage(mine_obj, j*bl_w, i*bl_h, bl_w, bl_h);
                else if (field[i][j] === '*' && flag_field[i][j]){
                    canvas.fillStyle = 'lightgreen';
                    canvas.strokeStyle = 'green';
                    canvas.beginPath();
                    canvas.rect(j*bl_w, i*bl_h, bl_w, bl_h);
                    canvas.fill();
                    canvas.stroke();
                    canvas.drawImage(flag_obj, j*bl_w, i*bl_h, bl_w, bl_h);
                    canvas.closePath();
                }
    }

    function restoreStyles(){
        canvas.fillStyle = 'lightgrey';
        canvas.strokeStyle = 'blue';
    }

    function checkForWin(){
        var kol_flag = 0;
        var kol_open = 0;
        for(var i = 0; i < sq_rows; i++){
            for(var j = 0; j < sq_cols; j++){
                if(field[i][j] === '*' && flag_field[i][j]) kol_flag++;
                else if(field[i][j] === -1) kol_open++;
            }
        }
        if((kol_flag + kol_open) === sq_cols*sq_rows){
            canvas_obj.removeEventListener('click', doLeftClick, false);
            canvas_obj.removeEventListener('contextmenu', doRightClick, false);
            sounds.win.play();
            alert('Win!');
        }
    }

    function drawText(i, j) {
        canvas.beginPath();
        canvas.rect(j * bl_w, i * bl_h, bl_w, bl_h);
        canvas.font = bl_h + "px Arial";
        canvas.fillStyle = 'white';
        canvas.fill();
        canvas.strokeStyle = 'green';
        canvas.stroke();
        canvas.fillStyle = 'blue';
        canvas.fillText(field[i][j], j * bl_w + bl_w / 4, (i + 1) * bl_h - bl_h / 8);
        field[i][j] = -1;
        canvas.closePath();
    }

}